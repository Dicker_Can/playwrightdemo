import { test, expect } from '@playwright/test';
import { webkit, firefox, chromium, Page } from 'playwright';

test.describe('playwright demo test', () => {
    const username = 'MMDEV';
    const password = 'porsche';

    test.beforeEach(async ({page}) => {        
        await page.goto('http://localhost:4200/');
    });
    test('basic test', async ({ page }) => {
        const title = await page.title();
        expect(title).toBe('Webconfig');
    }); 

    test('login', async ({ page }) => {
        await page.fill('#username-input', username);
        await page.fill('#password-input', password);
        await page.click('#signInBtn');
        await page.waitForTimeout(1000);
        expect(await page.screenshot()).toMatchSnapshot('after-login.png', {threshold: 0.1});
    });

    async function wait(page: Page, time) {
        await page.evaluate(() => {
            // if this doesn't work, you can try to increase 0 to a higher number (i.e. 100)
            return new Promise((resolve) => setTimeout(resolve, time));
        });
    }
})